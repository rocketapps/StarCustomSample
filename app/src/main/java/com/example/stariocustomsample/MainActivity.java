package com.example.stariocustomsample;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.starmicronics.stario.PortInfo;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.stario.StarPrinterStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity {

    private static final String TAG = "custom_sample";

    private String portName;
    private String portSettings;
    List<PortInfo> portInfoList;
    private int paperWidth = 384;
    private StarIOPort starIOPort;

    private Button printButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        printButton = (Button) findViewById(R.id.print_button);
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "Magic will happen", Toast.LENGTH_LONG).show();
                try {
                    portInfoList = StarIOPort.searchPrinter("USB:", getBaseContext());
                    for (PortInfo port : portInfoList) {
                        portName = port.getPortName();
                        portSettings = getPortSettingsOption(portName);

                        Log.i(TAG, "onClick: portName: " + portName + " portSettings  " + portSettings);
                    }

                    ArrayList<byte[]> commands = new ArrayList<byte[]>();

//                    RasterDocument document = new RasterDocument(
//                            RasterDocument.RasSpeed.Medium,
//                            RasterDocument.RasPageEndMode.FeedAndFullCut,
//                            RasterDocument.RasPageEndMode.FeedAndFullCut,
//                            RasterDocument.RasTopMargin.Standard, 0, 0, 0);

                    StarBitmap starBitmap = new StarBitmap(textAsBitmap("Sample text", 125), true, paperWidth);

                    commands.add(starBitmap.getImageRasterDataForPrinting_graphic(false));
                    commands.add(new byte[]{0x1b, 0x64, 0x02});

                    starIOPort = StarIOPort.getPort(portName, portSettings, 10000, getBaseContext());

                    StarPrinterStatus printerStatus = starIOPort.beginCheckedBlock();
                    Log.i(TAG, "onClick: PRINTER STATUS: " + printerStatus.toString());
                    byte[] commandToSenToPrinter = convertFromListByteArrayTobyteArray(commands);
                    starIOPort.writePort(commandToSenToPrinter, 0, commandToSenToPrinter.length);
                    starIOPort.setEndCheckedBlockTimeoutMillis(30000);
                    StarPrinterStatus printerStatusEnd = starIOPort.endCheckedBlock();
                    Log.i(TAG, "onClick: PRINTER END STATUS: " + printerStatusEnd.toString());
                } catch (StarIOPortException e) {
                    e.printStackTrace();
                } finally {
                    if (starIOPort != null) {
                        try {
                            StarIOPort.releasePort(starIOPort);
                        } catch (StarIOPortException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private String getPortSettingsOption(String portName) {

        String portSettings = "portable";

        if((Build.VERSION.SDK_INT == 14) || (Build.VERSION.SDK_INT == 15) ||(Build.VERSION.SDK_INT == 16)){
            portSettings += ";u";
        }

        return portSettings;
    }







    public Bitmap textAsBitmap(String text, int textColor) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(36);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent();
        int width = (int) (paint.measureText(text) + 0.5f);
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    private static byte[] convertFromListByteArrayTobyteArray(List<byte[]> ByteArray) {
        int dataLength = 0;
        for (int i = 0; i < ByteArray.size(); i++) {
            dataLength += ByteArray.get(i).length;
        }

        int distPosition = 0;
        byte[] byteArray = new byte[dataLength];
        for (int i = 0; i < ByteArray.size(); i++) {
            System.arraycopy(ByteArray.get(i), 0, byteArray, distPosition, ByteArray.get(i).length);
            distPosition += ByteArray.get(i).length;
        }

        return byteArray;
    }

}
